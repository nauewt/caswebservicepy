import cherrypy
import CASAuth
import simplejson

class Root(object):
    pass

class Uid(object):
    exposed = True
    def GET(self,callback,**params):
        ret = cherrypy.session.get('user')
        return '%s(%s)' % (callback, simplejson.dumps(ret))

root = Root()

root.uid = Uid()

conf = {
    'global': {
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8000,
        'tools.sessions.on': True,
        'tools.CASAuth.on': True,
        'tools.CASAuth.cas_server': "https://cas-test.nau.edu"
    },
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
    }
}

#create a custom cherrypy tool that will authenticate all page handlers using CAS
cherrypy.tools.CASAuth = cherrypy.Tool('before_handler', CASAuth.CASAuth)

cherrypy.quickstart(root, '/', conf)