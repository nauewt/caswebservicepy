import cherrypy
import xml.etree.ElementTree
import urllib

def CASAuth(cas_server):
    service_url = cherrypy.url(qs=cherrypy.request.query_string)
    if cherrypy.session.get('user'):
        #DEBUG: user is validated by existing session cookie
        cherrypy.session['validated_by'] = "session attribute"
        pass
    
    elif 'ticket' in cherrypy.request.params:
        service_url = cherrypy.session['service_url']
        #need to validate ticket
        ticket = cherrypy.request.params['ticket']
        
        #generate URL for ticket validation 
        cas_validate = cas_server + "/cas/serviceValidate?ticket=" + ticket + "&service=" + service_url
        f_xml_assertion = urllib.urlopen(cas_validate)
        if not f_xml_assertion:
            raise cherrypy.HTTPError(401, 'Unable to authenticate: trouble retrieving assertion from CAS to validate ticket.')

        #parse CAS XML assertion into a ElementTree
        assertion_tree = xml.etree.ElementTree.parse(f_xml_assertion)
        
        if not assertion_tree:
            raise cherrypy.HTTPError(401, 'Unable to authenticate: trouble parsing XML assertion.')
        user_name=None
        #find <cas:user> in ElementTree
        for e in assertion_tree.iter():
            #print "DEBUG: Found tag '%s' with text '%s'" % (e.tag,e.text)
            if e.tag != "{http://www.yale.edu/tp/cas}user":
                continue
            user_name = e.text
        if not user_name:
            #couldn't find <cas:user> in the tree
            raise cherrypy.HTTPError(401, 'Unable to validate ticket: could not locate cas:user element.')
    
        #add username to session
        cherrypy.session['user'] = user_name
        

        #DEBUG: user is validated by initial ticket instance
        cherrypy.session['validated_by'] = "ticket"
        
        #option a
        #leaves ticket=..... param in browser's URL
        del cherrypy.request.params['ticket']

        #option b
        #redirect to a clean service URL (without ticket=... param)
        #note: may cause usability issues if accessing a URL other than
        #initial Service URL with an expired session
        raise cherrypy.HTTPRedirect(service_url)
    
    else:
        #no existing session; no ticket to validate
        #redirect to CAS to retrieve new ticket
        cherrypy.session['service_url'] = service_url
        raise cherrypy.HTTPRedirect(cas_server + "/cas/login?service=" + service_url, 302)